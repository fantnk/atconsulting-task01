package top.fedoseev.task01;

import top.fedoseev.task01.pets.Pet;

import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Служит для хранения информации о животных в приюте,
 * реализует методы для {@link #addPet(Pet) добавления} животных в приют,
 * {@link #getPets(Class) получения списка} животных,
 * {@link #transferLongtimePet(Person) передачи} животного человеку.
 *
 * @see Pet
 * @see Person
 */

class Shelter {

    private final Set<Pet> pets;

    Shelter() {
        pets = new HashSet<>();
    }

    /**
     * Помещение животного в приют.
     *
     * @throws NullPointerException если передан параметр {@code null}
     */
    @SuppressWarnings("unchecked")
    void addPet(Pet pet) {
        Objects.requireNonNull(pet, "Pet can't be null. ");
        pets.add(pet);
        System.out.println(pet + " is successfully placed in a shelter. ");
    }

    /**
     * Получение списка всех животных определенного типа.
     *
     * @param clazz класс животного.
     * @throws NullPointerException если передан параметр {@code null}
     */
    @SuppressWarnings("unchecked")
    <T extends Pet> List<T> getPets(Class<T> clazz) {
        Objects.requireNonNull(clazz, "Class can't be null. ");
        return pets
                .stream()
                .filter(clazz::isInstance)
                .map(clazz::cast)
                .sorted((o1, o2) -> o1.getName().compareTo(o2.getName()))
                .collect(Collectors.toList());
    }

    /**
     * Получение животного определенного типа, находящегося в приюте наибольшее время,
     * {@code null} если в приюте нет животных данного типа.
     *
     * @param clazz класс животного.
     * @throws NullPointerException если передан параметр {@code null}
     * @see #getLongtimePet()
     */
    private <T extends Pet> T getLongtimePet(Class<T> clazz) {
        Objects.requireNonNull(clazz, "Class can't be null. ");
        return pets
                .stream()
                .filter(clazz::isInstance)
                .map(clazz::cast)
                .sorted((o1, o2) -> o1.getAdmissionDate().compareTo(o2.getAdmissionDate()))
                .findFirst()
                .orElse(null);
    }

    /**
     * Получение животного, находящегося в приюте наибольшее время,
     * {@code null} если в приюте нет животных.
     *
     * @see #getLongtimePet(Class)
     */
    private Pet getLongtimePet() {
        if (pets.isEmpty()) {
            return null;
        }

        return pets
                .stream()
                .sorted((o1, o2) -> o1.getAdmissionDate().compareTo(o2.getAdmissionDate()))
                .findFirst()
                .orElseGet(null);
    }

    /**
     * Передача человеку животного, находящегося в приюте.
     *
     * @throws NullPointerException если передан хотя бы один параметр {@code null}
     * @see Person#addPet(Pet)
     */
    @SuppressWarnings("unchecked")
    private void transferPet(Person person, Pet pet) {
        Objects.requireNonNull(person, "Person can't be null. ");
        Objects.requireNonNull(pet, "Pet can't be null. ");

        person.addPet(pet);
        pets.remove(pet);
        System.out.println(pet + " successfully transferred. ");
    }

    /**
     * Передача человеку животного, находящегося в приюте.
     *
     * @throws NullPointerException если передан параметр {@code null}
     * @see #transferLongtimePet(Person, Class)
     */
    @SuppressWarnings("unchecked")
    void transferLongtimePet(Person person) {
        Objects.requireNonNull(person, "Person can't be null. ");

        Pet pet = getLongtimePet();

        if (pet == null) {
            System.out.println("No pets in the shelter. ");
            return;
        }

        transferPet(person, pet);
    }

    /**
     * Передача человеку животного определенного типа, находящегося в приюте.
     *
     * @throws NullPointerException если передан хотя бы один параметр {@code null}
     * @see #transferLongtimePet(Person)
     */
    @SuppressWarnings("unchecked")
    <T extends Pet> void transferLongtimePet(Person person, Class<T> clazz) {
        Objects.requireNonNull(person, "Person can't be null. ");
        Objects.requireNonNull(clazz, "Class can't be null. ");

        Pet pet = getLongtimePet(clazz);

        if (pet == null) {
            System.out.println("No " + clazz.getSimpleName() + " in the shelter. ");
            return;
        }

        transferPet(person, pet);

    }

    @Override
    public String toString() {
        return "Shelter{" +
                "pets=" + pets +
                '}';
    }
}
