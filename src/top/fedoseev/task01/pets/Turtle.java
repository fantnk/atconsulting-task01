package top.fedoseev.task01.pets;

import java.time.LocalDate;

/**
 * @see Pet
 */

public class Turtle extends Pet {
    public Turtle(String name, short age) {
        super(name, age);
    }

    public Turtle(String name, short age, LocalDate admissionDate) {
        super(name, age, admissionDate);
    }

}
