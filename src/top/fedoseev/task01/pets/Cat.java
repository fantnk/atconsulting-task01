package top.fedoseev.task01.pets;

import java.time.LocalDate;

/**
 * @see Pet
 */

public class Cat extends Pet {

    public Cat(String name, short age) {
        super(name, age);
    }

    public Cat(String name, short age, LocalDate admissionDate) {
        super(name, age, admissionDate);
    }
}
