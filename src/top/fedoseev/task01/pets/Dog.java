package top.fedoseev.task01.pets;

import java.time.LocalDate;

/**
 * @see Pet
 */

public class Dog extends Pet {
    public Dog(String name, short age) {
        super(name, age);
    }

    public Dog(String name, short age, LocalDate admissionDate) {
        super(name, age, admissionDate);
    }
}
