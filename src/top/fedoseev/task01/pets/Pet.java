package top.fedoseev.task01.pets;

import java.time.LocalDate;

/**
 * Служит для хранения информации о животном.
 */

public abstract class Pet {
    private final String name;
    private final short age;

    /** Дата помещения животного в приют */
    private final LocalDate admissionDate;

    Pet(String name, short age) {
        this.name = name;
        this.age = age;
        admissionDate = LocalDate.now();
    }

    Pet(String name, short age, LocalDate admissionDate) {
        this.name = name;
        this.age = age;
        this.admissionDate = admissionDate;
    }

    public String getName() {
        return name;
    }

    public short getAge() {
        return age;
    }

    public LocalDate getAdmissionDate() {
        return admissionDate;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", admissionDate=" + admissionDate +
                '}';
    }
}
