package top.fedoseev.task01;

import top.fedoseev.task01.pets.Cat;
import top.fedoseev.task01.pets.Dog;
import top.fedoseev.task01.pets.Turtle;

import java.time.LocalDate;
import java.util.Random;

class Main {

    public static void main(String[] args) {
        Shelter shelter = new Shelter();

        //Наполнение приюта тестовыми данными
        populateShelter(shelter);

        //Помещение в приют собаки с указанием клички и возраста
        shelter.addPet(new Dog("Барбос", (short) 12));
        shelter.addPet(new Cat("Том", (short) 8));
        shelter.addPet(new Turtle("Тортилла", (short) 80));
        System.out.println();

        Person person = new Person();

        //Просмотр всех животных определенного типа
        System.out.println("Cats (sorted by name): " + shelter.getPets(Cat.class));
        System.out.println("Dogs (sorted by name): " + shelter.getPets(Dog.class));
        System.out.println("Turtles (sorted by name): " + shelter.getPets(Turtle.class));
        System.out.println();

        //Передача человеку животного, находящегося в приюте наибольшее время
        shelter.transferLongtimePet(person);

        //Передача человеку животного определенного типа, находящегося в приюте наибольшее время
        shelter.transferLongtimePet(person, Turtle.class);
        shelter.transferLongtimePet(person, Dog.class);
        shelter.transferLongtimePet(person, Turtle.class);
        shelter.transferLongtimePet(person, Cat.class);
        shelter.transferLongtimePet(person, Turtle.class);
        System.out.println();

        //Просмотр животных у человека
        System.out.println("Person's pets: " + person.getPets());
        System.out.println();

        //Просмотр всех животных определенного типа
        System.out.println("Cats (sorted by name): " + shelter.getPets(Cat.class));
        System.out.println("Dogs (sorted by name): " + shelter.getPets(Dog.class));
        System.out.println("Turtles (sorted by name): " + shelter.getPets(Turtle.class));
    }

    //Наполнение приюта тестовыми данными
    private static void populateShelter(Shelter shelter) {
        shelter.addPet(new Cat("Муська", (short) 3, nextLocalDate()));
        shelter.addPet(new Cat("Рыжик", (short) 8, nextLocalDate()));
        shelter.addPet(new Dog("Тузик", (short) 10, nextLocalDate()));
        shelter.addPet(new Turtle("Шустрик", (short) 140, nextLocalDate()));
        shelter.addPet(new Cat("Мурзик", (short) 5, nextLocalDate()));
        shelter.addPet(new Dog("Бобик", (short) 2, nextLocalDate()));
        shelter.addPet(new Cat("Барсик", (short) 13, nextLocalDate()));
    }

    //Вспомогательный метод для получения LocalDate из захардкоженного промежутка
    private static LocalDate nextLocalDate() {
        long minDay = LocalDate.of(2012, 1, 1).toEpochDay();
        long maxDay = LocalDate.of(2016, 8, 31).toEpochDay();
        int days = (int) (maxDay - minDay);

        return LocalDate.ofEpochDay(minDay + new Random().nextInt(days));
    }
}
