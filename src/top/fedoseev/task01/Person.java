package top.fedoseev.task01;

import top.fedoseev.task01.pets.Pet;

/**
 * Служит для хранения информации о человеке и его животных.
 * @see Pet
 */

import java.util.Set;
import java.util.TreeSet;


class Person {
    private final Set<Pet> pets;

    Person() {
        pets = new TreeSet<>((o1, o2) -> o1.getName().compareTo(o2.getName()));
    }

    Set getPets() {
        return pets;
    }

    void addPet(Pet pet) {
        pets.add(pet);
    }
}
